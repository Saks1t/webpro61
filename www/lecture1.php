<!--
<?php
/*$txt = "Hello World!";
$number = 10;

echo $txt."<br>";
echo $number;*/

/*echo "Hello World!"
echo "<h4>This is a simple heading.</h4>";
echo "<h4 style='color: red;'>This is heading with style.</h4>";*/

/*$a = 123;
var_dump($a);
echo "<br>";*/

/*$color = array("Red", "Green", "Blue");
var_dump($color);
echo "<br>";*/

/*class greeting{
	public $str = "Hello Wolrd!";
	function show_greeting(){
		return $this->str;
	}
}
$message = new greeting;
var_dump($message);
echo "<br>";
echo $message->show_greeting();*/

}
?>
-->
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Example of PHP POST method</title>
</head>
<body>
<?php
if(isset($_GET["name"])){
  echo "<p>Hi, ". $_GET["name"] . "</p>";
}
?>
<form method="get" action="<?php echo $_SERVER["PHP_SELF"];?>">
	<label for="inputName">Name:</label>
	<input type="text" name="name" id="inputName">
	<input type="submit" value="Submit">
</form>
</body>
</html>